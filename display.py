#!/usr/bin/env python3

from PIL import Image, ImageDraw, ImageFont
from datetime import datetime, timedelta
import urllib.request

from io import BytesIO
import json
import sys

from os import listdir, environ
from os.path import isfile, join
import random

font = ImageFont.truetype("Fonts/OpenSans-SemiBold.ttf", 18)
font_large = ImageFont.truetype("Fonts/OpenSans-SemiBold.ttf", 40)

pictures_dir = "Pictures"
pictures = [f for f in listdir(pictures_dir) if isfile(join(pictures_dir, f))]

temp = None
condition = None

if 'WEATHER_KEY' in environ and 'WEATHER_POSTCODE' in environ:
    contents = urllib.request.urlopen("http://api.weatherapi.com/v1/forecast.json?key={}&q={}&days=1&aqi=no&alerts=no".
                                      format(environ['WEATHER_KEY'], environ['WEATHER_POSTCODE'])).read()
    contents = json.loads(contents)
    day = contents["forecast"]["forecastday"][0]["day"]
    temp = day["avgtemp_c"]
    condition = day["condition"]

w_size = 120
h_size = 50
padding = 20
shadow_offset = 2

def draw_reminders(image: Image) -> Image:
    reminders, soon = [], []

    with open('reminders.json', 'r') as file:
        reminders = json.loads(file.read())

    for reminder in reminders:
        present = datetime.now().date()
        next = datetime.strptime(reminder['next'], "%d/%m/%Y").date()

        # Only shows reminders for the day before
        if present == (next - timedelta(days=1)):
            soon.append(reminder)

        if present > next:
            reminder['next'] = (next + timedelta(weeks=reminder['every'])).strftime("%d/%m/%Y")

    with open('reminders.json', 'w') as file:
        file.write(json.dumps(reminders, indent=2))
        file.close()

    with_reminders = image

    rect = image.copy()
    rect_draw  = ImageDraw.Draw(rect)

    # The reminders are just a list of things we need to do soon
    # with some drop shadow.
    for index, reminder in enumerate(soon):
        buffer = (index * (w_size + padding))

        x = 600 - padding - w_size - buffer
        y = 448 - padding - h_size
        darker = tuple(int(i * 0.8) for i in reminder["colour"])

        # Add a nice drop shadow
        rect_draw.rounded_rectangle([
            (x + shadow_offset, y + shadow_offset),
            (x + w_size + shadow_offset, y + h_size + shadow_offset)],
            fill=darker,
            radius=20)

        rect_draw.rounded_rectangle([
            (x, y), (x + w_size, y + h_size)],
            fill=tuple(reminder["colour"]),
            radius=20)

        # Do a sort of prerender so that I can center align the text
        width = rect_draw.textlength(reminder["reminder"], font=font)
        position = x + (w_size - width) / 2, y + 10

        rect_draw.text(
            position,
            reminder["reminder"],
            font=font,
            fill=(50, 50, 50))
        
    with_reminders = Image.blend(
    with_reminders, rect, 0.8)
    
    return with_reminders

def draw_weather(image: Image):
    if temp is None or condition is None:
        return

    draw  = ImageDraw.Draw(image)

    condition_icon = Image.open(BytesIO(urllib.request.urlopen("http:{}".format(condition["icon"])).read()))

    temp_text = "{}°C".format(temp)

    large_w = max(draw.textlength(temp_text, font=font_large) + condition_icon.size[0] + padding / 2,
                  draw.textlength(condition['text'], font=font) + padding * 2)
    large_h = padding * 2 + font_large.size + font.size

    x = padding
    y = padding

    draw.rounded_rectangle([
            (x, y), (x + large_w, y + large_h)],
            fill=(139, 206, 232),
            radius=20)

    image.paste(condition_icon, (x, y), condition_icon)

    draw.text((x + condition_icon.size[0], y), temp_text, font=font_large,
              fill=(33, 93, 117))
    
    draw.text((x + padding, y + condition_icon.size[1]), condition['text'], font=font,
              fill=(33, 93, 117))

def draw_screen(image: Image):
    is_local = len(sys.argv) > 1 and sys.argv[1] == "local"

    max_size = (600, 448)
    fill_color = (255, 255, 255)

    # We first resize the picture if the proportions are wrong.
    # The fill color will then take up the rest of the space.
    image.thumbnail(max_size)
    (width, height) = im.size
    with_background = Image.new('RGB', max_size, fill_color)
    with_background.paste(image, (
        int(max_size[0] / 2 - width / 2),
        int(max_size[1] / 2 - height / 2)))

    with_reminders = draw_reminders(with_background)

    draw_weather(with_reminders)

    # Then draw out to the screen locally, or to the display if on the raspberry pi.
    if is_local:
        with_reminders.show()
    else:
        from inky.auto import auto
        display = auto()
        display.set_image(with_reminders)
        display.show()


with Image.open(join(pictures_dir, random.choice(pictures))).convert("RGBA") as im:
    draw_screen(im)
