# Kitchen display

A small raspberry pi project for my kitchen to show pictures, and remind me to take out the bins.

I'm using the http://pimoroni.com/impression for this project.

I just run this script at midnight with a cron job.

I use https://www.weatherapi.com/ for weather data.
Create an environment variable called `WEATHER_KEY` with your key, and `WEATHER_POSTCODE` with your postcode.

## Reminders data format

- `reminder`: What you are getting reminded for
- `colour`: The colour of the dot for the reminder
- `next`: The next date the event will happen
- `every`: Frequency in weeks of event

eg:
```json
[
  {
    "reminder": "Food waste",
    "colour": [
      201,
      253,
      255
    ],
    "next": "10/03/2024",
    "every": 1
  }
]
```